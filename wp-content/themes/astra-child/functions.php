<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );


add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_style( 'style-name', get_stylesheet_directory_uri()."/base.min.css", array(), "", false);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    //wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
 function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

 //Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );
//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumb() {
    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
    }
}
add_action( 'astra_header_after', 'bbtheme_yoast_breadcrumb' );
function new_year_number()
{
return $new_year = date('Y');
}
add_shortcode('year_code', 'new_year_number');

//Covid home page banner hook
function bbtheme_covid_banner_custom() {
    $iscovid  =  get_option('covid');
     if ( $iscovid == '1' &&  is_front_page() ) {

          $content = do_shortcode('[fl_builder_insert_layout slug="covid19-banner-row"]');
        
     }
     echo $content;
}
add_action( 'astra_header_after', 'bbtheme_covid_banner_custom' );


function mmsession_custom_footer_js_astratheme() {
     
    echo "<script src='https://session.mm-api.agency/js/mmsession.js' async></script>";
}
add_action( 'wp_footer', 'mmsession_custom_footer_js_astratheme' );

function year_shortcode() {
  $year = date('Y');
  return $year;
}
add_shortcode('year', 'year_shortcode');

class FL_Cache_Buster {
	public static function init() {
		add_action( 'upgrader_process_complete',           array( __class__, 'clear_caches' ) );
		add_action( 'fl_builder_after_save_layout',        array( __class__, 'clear_caches' ) );
		add_action( 'fl_builder_after_save_user_template', array( __class__, 'clear_caches' ) );
		add_action( 'fl_builder_cache_cleared',            array( __class__, 'clear_caches' ) );
		add_action( 'template_redirect',                   array( __class__, 'donotcache' ) );
	}
	/**
	 * Clear the various cache plugins.
	 * @since 1.0
	 */
	public static function clear_caches() {
		//rocket cache
		if ( function_exists( 'rocket_clean_domain' ) ) {
			rocket_clean_domain();
		}
		// wp-super-cache
		if ( function_exists( 'wp_cache_clear_cache' ) ) {
			wp_cache_clear_cache();
		}
		// WPEngine
		if ( class_exists( 'WpeCommon' ) ) {
			WpeCommon::purge_memcached();
			WpeCommon::clear_maxcdn_cache();
			WpeCommon::purge_varnish_cache();
		}
		// w3 total crash
		if ( function_exists( 'w3tc_pgcache_flush' ) ) {
			w3tc_pgcache_flush();
		}
		// siteground
		if ( function_exists( 'sg_cachepress_purge_cache' ) ) {
			sg_cachepress_purge_cache();
		}

		// varnish
		@wp_remote_request( get_site_url(), array( 'method' => 'BAN' ) );

		// LiteSpeed
		if( class_exists( 'LiteSpeed_Cache_API' ) ) {
			LiteSpeed_Cache_API::purge_all();
		}

		// Cache Enabler
		if( class_exists( 'Cache_Enabler' ) ) {
			Cache_Enabler::clear_total_cache();
		}

		// Pagely
		if ( class_exists( 'PagelyCachePurge' ) ) {
			PagelyCachePurge::purgeAll();
		}

		// wp fastest cache
		if( class_exists( 'WpFastestCache' ) ) {
			global $wp_fastest_cache;
			$wp_fastest_cache->deleteCache( true );
		}

		// autoptimize
		if( class_exists( 'autoptimizeCache' ) ) {
			autoptimizeCache::clearall();
		}
		//comet cache (formerly zencache)
		if( class_exists( 'comet_cache' ) ) {
			comet_cache::clear();
		}
		error_log( 'Cleared Caches' );
	}
	/**
	 * Set DONOTCACHEPAGE if builder is active.
	 * @since 1.0
	 */
	public static function donotcache() {
		if ( ! defined( 'DONOTCACHEPAGE' )
			&& class_exists( 'FLBuilderModel' )
			&& FLBuilderModel::is_builder_active() ) {
				define( 'DONOTCACHEPAGE', true );
		}
	}
}
FL_Cache_Buster::init();



class Over_FL_Filesystem extends FL_Filesystem {

	function file_get_contents( $path ) {
		return file_get_contents( $path );
	}

	function file_exists( $path ) {
		return file_exists( $path );
	}
}

add_action( 'plugins_loaded', function() {
    add_filter( 'fl_filesystem_instance', function() {
        return new Over_FL_Filesystem;
        }
    );
}
);