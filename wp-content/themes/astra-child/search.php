<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>
	<div class="search-row">
		<?php astra_primary_content_top(); ?>

		<?php astra_archive_header(); ?>

		<?php
		if(have_posts()) :
		include( ABSPATH .'/wp-content/plugins/grand-child/product-listing-templates/product-loop-search.php' );
		else :
        ?>
		<?php astra_content_loop(); ?>		
		<?php endif; ?>
		<?php astra_pagination(); ?>

		<?php astra_primary_content_bottom(); ?>
	</div>		
	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>